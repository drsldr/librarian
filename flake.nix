{
  description = "Librarian development environment";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
      pyenv = pkgs.python3.withPackages (ps: with ps; [
        black
        click
        coverage
        pylint
        pytest
        requests
        sqlalchemy
      ]);
    in
    {
      devShell.x86_64-linux = with pkgs; mkShell {
        name = "librarian-sh";
        buildInputs = [
          pyenv
          ffmpeg
          zip
          sqlitebrowser
        ];
        shellHook = ''
          python --version;
          ffmpeg -version | head -1;
        '';
      };
    };
}
