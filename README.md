# Librarian

A Python utility for managing a store of sheet music and related artifacts.

## Why, though?

I am probably unique in having the problem Librarian is trying to solve - I assist a
choir with tidying up sheet music and generate audio from said sheet music that can be
used for rehearsal. It's gotten to the point where I'd like to be able to automate a lot
of that work, and I did originally write an (awful) bash-script to handle some of the
tedium. That made a lot of assumptions that no longer hold, however, and as an exercise
to Git Gud at Python again, I'm rebuilding from scratch.

While I may be alone in this specific problem, there's really no reason to close-source
this, so here it is.

## License

This project is licensed under the MIT License (see LICENSE file).
